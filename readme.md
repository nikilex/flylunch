**Установка**
* 1. `git clone git@bitbucket.org:nikilex/flylunch.git`
* 2. `cd flylunch; npm install`
* 3. `cd www; php -S localhost:8000`

Лендинг будет доступен по адерсу: http://localhost:8000/landing.html

Исходники 
* src/js
* src/scss
* project/

Компиляция 
* gulp js
* gulp scss
* gulp fileinclude
* gulp watch (в реальном времени)
