
var landing = {
    host: '',
    protocol: '',
    data: {

    },
    selectors: {
        get inputSubdomain() { return '#subdomain'; },
        get popupEnter() { return '#enter'; }
    },
    init: function () {
        var that = this;
        window.landing = that;
        that.host = window.location.host;
        that.protocol = window.location.protocol;
        that.scroll();
        that.phoneMask();
        that.topSticky('#top');
        that.changePrice();
        that.changePriceHelper(1380,4150,7815,'При оплате за 6 месяцев');
        

        $(that.selectors.popupEnter).on('opened', function () {
            that.popupEnterOpened();
        });

        $(that.selectors.inputSubdomain).on('keyup', function (e) {
            if(e.which == 13) { that.goSubdomain(); }
        });

    },
    goSubdomain: function() {
        var val = $(this.selectors.inputSubdomain).val();
        if (!val) {
            alert('Укажите поддомен');
            return false;
        }
        window.location.href = '?subdomain=' + val;
    },
    popupEnterOpened: function () {
        $(this.selectors.inputSubdomain).focus();
    },
    popupOpen: function (id) {
        $('#' + id).addClass('open').trigger('opened');
    },
    popupClose: function (id) {
        $('#' + id).removeClass('open');
    },
    activateButton: function ($this) {
        console.log($(this));
        $(this).parent().children("button").removeClass('active');
        $(this).addClass('cative');
    },
    phoneMask: function () {
        $("#form-phone").inputmask("999 999-99-99", {showMaskOnFocus: false, showMaskOnHover: false});
        $("#modal-phone").inputmask("999 999-99-99", {showMaskOnFocus: false, showMaskOnHover: false});
        $("#modal-phone2").inputmask("999 999-99-99", {showMaskOnFocus: false, showMaskOnHover: false});
    },
    topSticky: function(selector) {
        $(selector).scroolly([
            {
                from: 'con-top',
                to: 'con-top + 250 = top',
                css: {
                    opacity: 0,
                }
            },
            {   
                from: 'con-top + 250 = top',
                to: 'con-bottom = top',
                css: {
                    position: '-webkit-sticky',
                    position: "sticky",
                    top: '0px',
                    bottom: '',
                    opacity: 1,
                    transition: '.35s',
                },

            },
        ], $('body'));
    },
    scroll: function(){
        $(".top .links").on("click","a", function (event) {
            //отменяем стандартную обработку нажатия по ссылке
            event.preventDefault();
    
            //забираем идентификатор бока с атрибута href
            var id  = $(this).attr('href');
            
            //узнаем высоту от начала страницы до блока на который ссылается якорь
            if ($(id).length > 0)
            {
                var top = $(id).offset().top;
            
                //анимируем переход на расстояние - top за 1500 мс
                $('body,html').animate({scrollTop: top-70}, 1500);
            }
        });
    },
    changePriceHelper: function(base, basePlus, pro, message) {
                    $('.base-price').html(base);
                    $('.base-plus-price').html(basePlus);
                    $('.pro-price').html(pro);
                    $('.price-message').html(message);
    },
    changePrice: function() {
        $('.btn-change-price').on('click', function() {
            switch ($(this).data('round')) {
                case 1:
                    window.landing.changePriceHelper(1500,4500,8500,'');;
                    $('.price-message').hide();
                    break;
                case 3:
                    window.landing.changePriceHelper(1430,4280,8100,'При оплате за квартал');
                    $('.price-message').show();
                    break;
                case 6:
                    window.landing.changePriceHelper(1380,4150,7815,'При оплате за 6 месяцев');
                    $('.price-message').show();
                    break;
                case 12:
                    window.landing.changePriceHelper(1315,3960,7480,'При оплате за год');
                    $('.price-message').show();
                    break;
            }
        })
    },

}.init();

var registration = {
    countryCodeData: {
        'ru': {
            code: '+7',
            numberCount: '10', 
            mask: '999 999-99-99', 
            placeholder: '912 238-18-58',
            flagURL: '',
        },

        'kz': {
            code: '+7',
            numberCount: '10', 
            mask: '999 999-99-99', 
            placeholder: '771 000-99-98',
        },

        'ua': {
            code: '+380',
            numberCount: '9', 
            mask: '99 999-99-99', 
            placeholder: '39 123-45-67',
        },

        'by': {
            code: '+375',
            numberCount: '9', 
            mask: '99 999-99-99', 
            placeholder: '22 491-19-11',
        },

        'uz': {
            code: '+998',
            numberCount: '9', 
            mask: '99 999-99-99', 
            placeholder: '91 234-56-78',
            flagURL: '',
        },

        'af': {
            code: '+93',
            numberCount: '9', 
            mask: '99 999-99-99', 
            placeholder: '70 123-45-67',
        }
    },

    init: function () {
        var that = this;
        window.registration = that;
        that.onInputEvent();
        that.initCoockie();
        that.validate();
        that.selectCodePhone();
        that.filterManufacture();

        if (localStorage.getItem('client-country-code') == null) {
            localStorage.setItem('client-country-code', 'ru') 
        }

        window.registration.phoneCodeChange("#modal-phone", localStorage.getItem('client-country-code'), true);
        window.registration.phoneCodeChange("#form-phone", localStorage.getItem('client-country-code'), true);
    },

    initCoockie: function () {
        var phoneInputForm = $('#modal-phone'),
            nameInputForm  = $('.modal-name'),
            formName       = $('#form-name'),
            formPhone      = $('#form-phone');
            email          = $('#email-modal');
            manufacture    = $('#manufacture-modal');
            phoneCode      = $('.phone-code');

        phoneInputForm.val(localStorage.getItem('client-phone'));
        nameInputForm.val(localStorage.getItem('client-name'));
        formName.val(localStorage.getItem('client-name'));
        formPhone.val(localStorage.getItem('client-phone'));
        email.val(localStorage.getItem('client-email'));
        manufacture.val(localStorage.getItem('client-manufacture'));
        phoneCode.val(localStorage.getItem('phone-code'));
    },

    phoneCodeChange: function(selector, countryCode = "ru", valid = false) {
    
            var phoneInputs = $(selector);
            var dataForPhoneInput = window.registration.countryCodeData[countryCode];

            $('.phone-code').html(dataForPhoneInput.code);

            phoneInputs.attr('placeholder', dataForPhoneInput.placeholder);
            phoneInputs.inputmask(dataForPhoneInput.mask, {showMaskOnFocus: false, showMaskOnHover: false});
            phoneInputs.rules('add', { minlengthPhone: dataForPhoneInput.numberCount});
            $('.icon-flag-phone').removeClass().addClass('icon-flag-phone phone-code-flag mx-1 '+countryCode);
            if (valid) {
              phoneInputs.valid();  
            }        
            localStorage.setItem('client-country-code', countryCode);       
    },

    filterManufacture: function(selector = "#manufacture-modal", selectorLabel = "#manufacture-modal-label") {
        $(selector).on('input', function() {
            if(window.registration.toTranslit($(selector).val()).length <= 50) {
                $(selectorLabel).html("Ваш адрес на Flylunch: "+window.registration.toTranslit($(selector).val()) + ".flylunch.ru"); 
                localStorage.setItem('client-manufacture', window.registration.toTranslit($(selector).val()));
            }
            if ($(selector).val().length < 1) {
                $(selectorLabel).html('');
            }   
        })
    },

    toTranslit: function(text) {
        return text.replace(/([а-яё])|([\s_-])|([^a-z\d])/gi,
        function (all, ch, space, words, i) {
            if (space || words) {
                return space ? '-' : '';
            }
            var code = ch.charCodeAt(0),
                index = code == 1025 || code == 1105 ? 0 :
                    code > 1071 ? code - 1071 : code - 1039,
                t = ['yo', 'a', 'b', 'v', 'g', 'd', 'e', 'zh',
                    'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
                    'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh',
                    'shch', '', 'y', '', 'e', 'yu', 'ya'
                ]; 
            return t[index];
        }).toLowerCase().replace(/^[\s\-]+/g, '').replace(/[\s\-]+$/g, '');
    },

    selectCodePhone: function() {
        $('.phone-code-item').on('click', function() {
            window.registration.phoneCodeChange("#modal-phone", $(this).data('country'), true);
            window.registration.phoneCodeChange("#form-phone", $(this).data('country'), true);
        })
        
    },

    validate: function () {
        $.validator.addMethod("requiredPhone", function(value, element, param) {
            value = value.replace('+7 (___) ___ - __ - __');
            
            if ( !this.depend( param, element ) ) {
                return "dependency-mismatch";
            }
            if ( element.nodeName.toLowerCase() === "select" ) {
                var val = $( element ).val();
                return val && val.length > 0;
            }
            if ( this.checkable( element ) ) {
                return this.getLength( value, element ) > 0;
            }
            return $.trim( value ).length > 0;
        }, "Укажите номер телефона");
        
        $.validator.addMethod("minlengthPhone", function(value, element, param) {
            value = value.replace('+7', '');
            value = value.replace(/\s/g, '');
            value = value.replace(/-/g, '');
            value = value.replace(/_/g, '');
            
            var length = $.isArray( value ) ? value.length : this.getLength( $.trim( value ), element );
            return this.optional( element ) || length >= param;
        }, "Телефон должен быть не менее {0} символов");

        $("#try-form").validate({
            focusInvalid: false,
            errorClass: 'error',
            rules: {
                name: {
                    required: true,
                },
                phone: {
                    requiredPhone: true,
                    minlengthPhone: 10,
                }
              },
              messages: {
                name: "Напишите имя, чтобы знали как к вам обращаться",
              },
              submitHandler: function(form) {
                  $('#registrationModal').modal('show');
              },
        });

        $("#step-1-form").validate({
            focusInvalid: false,
            errorClass: 'error',
            rules: {
                name: {
                    required: true,
                },
                phone: {
                    requiredPhone: true,
                    minlengthPhone: 10,
                },
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    minlength: 6,
                },
                contract: {
                    required: true,
                }
              },
              messages: {
                name: "Напишите имя, чтобы знали как к вам обращаться",
                email: {
                    required: "Проверьте адрес эл. почты, который вы написали",
                    email: "Неверный формат эл. почты"
                },
                password: "Напишите пароль минимум из 6 символов",
                contract: "Без согласия нельзя продолжить регистрацию"
              },
              submitHandler: function() {
                  $('#step-1-tab').tab('show')
              },
              errorPlacement: function(error, element) {
                  error.each(function (index , item) {
                      if ($(item).attr("id") == "contract-error")
                      {
                       $(item).appendTo( $('#personalContractCheck').parent().parent() )
                      } else {
                        $(item).appendTo( $(element).parent() )
                      }
                  })
               ;
              }
        });

        $('#step-2-form').validate({
            focusInvalid: false,
            errorClass: 'error',
            rules: {
                buisnes_is_work: {
                    required: true,
                },
                manufacture_name: {
                    required: true,
                },
                role_in_company: {
                    required: true,
                },
              },
              messages: {
                buisnes_is_work: "Укажите статус",
                manufacture_name: "Укажите статус",
                role_in_company: "Укажите статус"
              },
              submitHandler: function() {
                  console.log('ok');
                  $('#registrationModal').modal('hide');
                  window.registration.clearInputs();

              },
        })

    },

    onInputEvent: function () {
        var phoneInputForm = $('#modal-phone'),
            nameInputForm  = $('.modal-name'),
            formName       = $('#form-name'),
            formPhone      = $('#form-phone');
            email          = $('#email-modal');
            manufacture    = $('#manufacture-modal');

            formPhones = $('.phone-input');

        phoneInputForm.on('input', function() {
            localStorage.setItem('client-phone', window.registration.replacePhone(phoneInputForm.val()))
            formPhone.val(window.registration.replacePhone(phoneInputForm.val()));
        })
        nameInputForm.on('input', function() {
            localStorage.setItem('client-name', nameInputForm.val())
            formName.val(nameInputForm.val());
        })

        formName.on('input', function() {
            localStorage.setItem('client-name', formName.val());
            nameInputForm.val(formName.val());
        })

        formPhone.on('input', function() {
            localStorage.setItem('client-phone', window.registration.replacePhone(formPhone.val()));
            phoneInputForm.val(window.registration.replacePhone(formPhone.val()));
        })

        email.on('input', function() {
            localStorage.setItem('client-email', email.val());
        })

        manufacture.on('input', function() {
            localStorage.setItem('client-manufacture', manufacture.val());
        })
        
    },

    replacePhone: function (value) {
        value = value.replace('+7', '');
        value = value.replace(/\s/g, '');
        value = value.replace(/-/g, '');
        value = value.replace(/_/g, '');
        return value;
    },

    clearInputs: function () {
        var phoneInputForm = $('#modal-phone'),
            nameInputForm  = $('.modal-name'),
            formName       = $('#form-name'),
            formPhone      = $('#form-phone');
            email          = $('#email-modal');
            manufacture    = $('#manufacture-modal');
        
        phoneInputForm.val('');
        nameInputForm.val('');
        formName.val('');
        formPhone.val('');
        email.val('');
        manufacture.val('');

        localStorage.setItem('client-phone','');
        localStorage.setItem('client-name','');
        localStorage.setItem('client-name','');
        localStorage.setItem('client-phone','');
        localStorage.setItem('client-email','');
        localStorage.setItem('client-manufacture','');
    }

}.init();

var capabilities = {
    init: function () {
        var that = this;
        window.capabilities = that;
        $('.btn-activate').click(function(){
            $(this).parent().children("button").removeClass('active');
            $(this).addClass('active');
        }) 
        that.initCarouselIndicators();
    },
    initCarouselIndicators: function() {
        $(".buttons[data-target]").each(function (i, indicators) {
            var targetId = indicators.dataset.target;
            if (targetId != "") {
                var $carousel = $(targetId);
                $carousel.bind('slide.bs.carousel', function (e) {
                    var $targetSlide = $(e.relatedTarget);
                    var index = $targetSlide.index();
                    $('.buttons[data-target="' + targetId + '"] .btn-activate').removeClass('active')
                    $('.buttons[data-target="' + targetId + '"] .btn-activate:nth-child(' + (index + 1) + ')').addClass('active');
                });
            }
        });
    }
}.init();